package com.jinu.moviewalker.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PageKeyedDataSource;
import android.arch.paging.PagedList;

import com.jinu.moviewalker.model.Movie;
import com.jinu.moviewalker.ui.paging.MovieDataSource;
import com.jinu.moviewalker.ui.paging.MovieDataSourceFactory;

public class MovieViewModel extends ViewModel {

    //creating livedata for PagedList  and PagedKeyedDataSource
    private LiveData<PagedList<Movie>> moviePagedList;
    private LiveData<String> progressLoadStatus = new MutableLiveData<>();
    LiveData<PageKeyedDataSource<Integer, Movie>> liveDataSource;

    public MovieViewModel() {
        MovieDataSourceFactory movieDataSourceFactory = new MovieDataSourceFactory();


        //Getting PagedList config
        PagedList.Config pagedListConfig =
                (new PagedList.Config.Builder())
                        .setEnablePlaceholders(false)
                        .setPageSize(MovieDataSource.PAGE_SIZE).build();

        //Building the paged list
        moviePagedList = new LivePagedListBuilder(movieDataSourceFactory, pagedListConfig).build();

        progressLoadStatus = Transformations.switchMap(movieDataSourceFactory.getMutableLiveData(), MovieDataSource::getProgressLiveStatus);
    }

    public LiveData<String> getProgressLoadStatus() {
        return progressLoadStatus;
    }

    public LiveData<PagedList<Movie>> getMoviePagedList() {
        return moviePagedList;
    }
}
