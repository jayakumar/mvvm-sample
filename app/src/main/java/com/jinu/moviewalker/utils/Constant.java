package com.jinu.moviewalker.utils;

/**
 * This class will have constant values, those are used across the application
 */
public class Constant {
    // The loading constant is not translatable string used to check the loading satus of data from network
    public final static String LOADING = "Loading";

    // This constant is not translatable string used to check the loading satus of data from network
    public final static String LOADED = "Loaded";

}
