package com.jinu.moviewalker.utils;

/**
 * All the network url strings will be configured here.
 *
 * @author jinu j
 */
public class UrlConfigurations {

    // Base url to the backed server
    public static final String BASE_URL = "http://api.themoviedb.org/3/";

    // image url
    public static final String IMAGE_DOWNLOAD_URL = "https://image.tmdb.org/t/p/w300";
}
