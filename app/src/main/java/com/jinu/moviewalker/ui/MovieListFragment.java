package com.jinu.moviewalker.ui;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jinu.moviewalker.R;
import com.jinu.moviewalker.databinding.FragmentMovieListBinding;
import com.jinu.moviewalker.ui.adapter.MovieAdapter;
import com.jinu.moviewalker.utils.Constant;
import com.jinu.moviewalker.viewmodels.MovieViewModel;

import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MovieListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MovieListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MovieListFragment extends Fragment {

    public static final String TAG = MovieListFragment.class.getSimpleName();

    private OnFragmentInteractionListener mListener;

    private MovieViewModel movieViewModel;

    private FragmentMovieListBinding binding;

    private View mRootView;

    private Activity mActivity;

    private MovieAdapter adapter;

    public MovieListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MovieListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MovieListFragment newInstance() {
        MovieListFragment fragment = new MovieListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_movie_list, container, false);

        //setting up recyclerview
        binding.recyclerview.setLayoutManager(new LinearLayoutManager(mActivity));
        binding.recyclerview.setHasFixedSize(true);
        //creating the Adapter
        adapter = new MovieAdapter(this.getContext());
        //setting the adapter
        binding.recyclerview.setAdapter(adapter);

        mRootView = binding.getRoot();
        return mRootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (Activity) context;
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    /**
     * Having following initialization tasks
     * getting our ItemViewMode
     * creating the Adapter
     * register observer for  the itemPagedList from view model
     * register observer to handle progress bar visibility
     */
    private void init() {



        //getting our ItemViewModel
        movieViewModel = ViewModelProviders.of(this.getActivity()).get(MovieViewModel.class);


        //observing the itemPagedList from view model
        movieViewModel.getMoviePagedList().observe(this.getActivity(), adapter::submitList);


        // register observer to handle progress bar visibility
        movieViewModel.getProgressLoadStatus().observe(this, status -> {
            if (Objects.requireNonNull(status).equalsIgnoreCase(Constant.LOADING)) {
                binding.progressbar.setVisibility(View.VISIBLE);
            } else if (Objects.requireNonNull(status).equalsIgnoreCase(Constant.LOADED)) {
                binding.progressbar.setVisibility(View.GONE);
            }
        });
    }
}
