package com.jinu.moviewalker.ui;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;

import com.jinu.moviewalker.R;
import com.jinu.moviewalker.network.ConnectivityChecker;

/**
 * Application main activity, all movie detail data are populated to this activity layout
 *
 * @author jinu j
 */
public class MainActivity extends AppCompatActivity implements MovieListFragment.OnFragmentInteractionListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        showMovieFragment();
        //getLifecycle().addObserver(new MyLifeCycle());
    }


    @Override
    protected void onResume() {
        super.onResume();

        /**
         * Once after application ui visible, checking network state and show snack bar if device not connected to network
         * This is not the best way to do network checking, in future this code snippet will be removed and write a scheduler job to check network state and send callback to ui to network failure
         */
        if (!ConnectivityChecker.checkInternetConnection(this)) {
            Snackbar.make(findViewById(android.R.id.content), R.string.network_error, Snackbar.LENGTH_SHORT)
                    .show();
        }
    }


    private void showMovieFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, MovieListFragment.newInstance(), MovieListFragment.TAG)
                .commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
