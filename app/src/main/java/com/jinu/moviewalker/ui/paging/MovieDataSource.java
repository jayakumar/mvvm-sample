package com.jinu.moviewalker.ui.paging;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.PageKeyedDataSource;
import android.support.annotation.NonNull;

import com.jinu.moviewalker.utils.Constant;
import com.jinu.moviewalker.model.Movie;
import com.jinu.moviewalker.model.MoviesResponse;
import com.jinu.moviewalker.network.api.ApiClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * MovieDataSource do the page wise loading
 *
 * @author jinu j
 */
public class MovieDataSource extends PageKeyedDataSource<Integer, Movie> {

    //we will start from the first page which is 1
    private static final int FIRST_PAGE = 1;

    private static final String KEY = "75e3e07c974131ec9abc9037485611ff";

    public static final int PAGE_SIZE = 20;

    // mutable live data holding loading status, observers use it's update to show and hide progress bar
    private MutableLiveData<String> progressLiveStatus;

    public MovieDataSource() {
        progressLiveStatus = new MutableLiveData<>();
    }

    public MutableLiveData<String> getProgressLiveStatus() {
        return progressLiveStatus;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull final LoadInitialCallback<Integer, Movie> callback) {
        progressLiveStatus.postValue(Constant.LOADING);
        ApiClient.getInstance().getMovieApi().getTopRatedMovies(KEY, FIRST_PAGE).enqueue(new Callback<MoviesResponse>() {
            @Override
            public void onResponse(Call<MoviesResponse> call, Response<MoviesResponse> response) {
                if (response.body() != null) {
                    MoviesResponse moviesResponse = response.body();
                    Integer adjacentKey = (FIRST_PAGE < moviesResponse.getTotalPages()) ? FIRST_PAGE + 1 : null;
                    callback.onResult(moviesResponse.getResults(), null, adjacentKey);
                }
                progressLiveStatus.postValue(Constant.LOADED);
            }

            @Override
            public void onFailure(Call<MoviesResponse> call, Throwable t) {
                progressLiveStatus.postValue(Constant.LOADED);
            }
        });
    }

    @Override
    public void loadBefore(@NonNull final LoadParams<Integer> params, @NonNull final LoadCallback<Integer, Movie> callback) {
        progressLiveStatus.postValue(Constant.LOADING);
        ApiClient.getInstance().getMovieApi().getTopRatedMovies(KEY, params.key).enqueue(new Callback<MoviesResponse>() {
            @Override
            public void onResponse(Call<MoviesResponse> call, Response<MoviesResponse> response) {
                if (response.body() != null) {
                    //if the current page is greater than one
                    //we are decrementing the page number
                    //else there is no previous page
                    Integer adjacentKey = (params.key > 1) ? params.key - 1 : null;
                    callback.onResult(response.body().getResults(), adjacentKey);
                }
                progressLiveStatus.postValue(Constant.LOADED);
            }

            @Override
            public void onFailure(Call<MoviesResponse> call, Throwable t) {
                progressLiveStatus.postValue(Constant.LOADED);
            }
        });
    }

    @Override
    public void loadAfter(@NonNull final LoadParams<Integer> params, @NonNull final LoadCallback<Integer, Movie> callback) {
        progressLiveStatus.postValue(Constant.LOADING);
        ApiClient.getInstance().getMovieApi().getTopRatedMovies(KEY, params.key).enqueue(new Callback<MoviesResponse>() {
            @Override
            public void onResponse(Call<MoviesResponse> call, Response<MoviesResponse> response) {
                if (response.body() != null) {
                    MoviesResponse moviesResponse = response.body();
                    Integer adjacentKey = (params.key < moviesResponse.getTotalPages()) ? params.key + 1 : null;
                    callback.onResult(response.body().getResults(), adjacentKey);
                }
                progressLiveStatus.postValue(Constant.LOADED);
            }

            @Override
            public void onFailure(@NonNull Call<MoviesResponse> call, Throwable t) {
                progressLiveStatus.postValue(Constant.LOADED);
            }
        });
    }
}
