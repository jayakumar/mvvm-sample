package com.jinu.moviewalker.ui.paging;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.DataSource;

import com.jinu.moviewalker.model.Movie;

/**
 * Having implementation to how to create data source with @{@link Movie}
 */
public class MovieDataSourceFactory extends DataSource.Factory<Integer, Movie> {

    /**
     * Creating the mutable live data
     */
    private MutableLiveData<MovieDataSource> movieLiveData;

    private MovieDataSource movieDataSource;

    public MovieDataSourceFactory() {
        movieDataSource = new MovieDataSource();
        movieLiveData = new MutableLiveData<>();
    }

    @Override
    public DataSource<Integer, Movie> create() {
        /**
         * posting the datasource to get the values
         */
        movieLiveData.postValue(movieDataSource);
        return movieDataSource;
    }

    /**
     * @return liveData
     */
    public MutableLiveData<MovieDataSource> getMutableLiveData() {
        return movieLiveData;
    }
}
