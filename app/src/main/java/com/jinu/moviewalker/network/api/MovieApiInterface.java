package com.jinu.moviewalker.network.api;

import com.jinu.moviewalker.model.MoviesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * All the movie rest api will be provided in this interface
 * @author jinu j
 */
public interface MovieApiInterface {

    /**
     * This api will return Top rated movies
     * @param apiKey generated key from http://api.themoviedb.org/
     * @param page pagination is doing acccording to this value
     * @return
     */
    @GET("movie/top_rated")
    Call<MoviesResponse> getTopRatedMovies(@Query("api_key") String apiKey, @Query("page") int page);

}
