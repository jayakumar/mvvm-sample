package com.jinu.moviewalker.network.api;

import com.jinu.moviewalker.utils.UrlConfigurations;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * ApliClient class have the implementation to create @{@link Retrofit} class object.
 *
 * @author jinu j
 */
public class ApiClient {


    private static volatile ApiClient mInstance;
    private Retrofit retrofit;

    /**
     * private constructor to controll the creation of object
     */
    private ApiClient() {
        retrofit = new Retrofit.Builder()
                .baseUrl(UrlConfigurations.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    /**
     * Double checked locking but avoiding unwanted locking
     *
     * @return singleton instance of ApiClient
     */
    public static ApiClient getInstance() {

        if (mInstance == null) {
            synchronized (ApiClient.class) {
                if (mInstance == null) {
                    mInstance = new ApiClient();
                }
            }
        }
        return mInstance;
    }

    /**
     * Create and give an implementation of the API endpoints defined by the MovieApiInterface interface.
     *
     * @return a {@link Call} which represents the HTTP request for Movie Api
     */
    public MovieApiInterface getMovieApi() {
        return retrofit.create(MovieApiInterface.class);
    }
}
